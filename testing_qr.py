from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import zbar.misc
import cv2
import zbar
import struct
import serial
 

global results
citit=60


def get_qr_code():
        print("apel qr")
        return citit

def qr_reader():
        camera = PiCamera()
        camera.resolution = (640, 480)
        camera.framerate = 32
        scanner = zbar.Scanner()
        rawCapture = PiRGBArray(camera, size=(640, 480))
        #ser=serial.Serial('/dev/ttyACM0',9600)
        time.sleep(0.1)
      
        global citit
        while True:
                for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
                        image = frame.array
                        #cv2.imshow("Frame", image)
                        if len(image.shape) == 3:
                                image = zbar.misc.rgb2gray(image)
                        results = scanner.scan(image)
                        if results==[]:
                                citit=60
                        else:
                            for result in results:
                                citit=result.data.decode("ascii")
                                time.sleep(1)
                        rawCapture.truncate(0)
                        # trimitem comanda ser.write(struct.pack('>B',citit))
                key = cv2.waitKey(0) & 0xff 
                if key == 27:
                        break
        camera.close()
        cv2.destroyAllWindows()
