import threading
import time
import testing_qr as reader
import serial
import struct

V = 5
drumCautat = [0] * 5
lungimeDrum = 0
noduriParcurse=[0] *30
ser=serial.Serial('/dev/ttyACM3',9600)
time.sleep(2)


def afisareDrum():
    global lungimeDrum
    global V

    lungimeDrum -= 1
    for i in range(lungimeDrum+1,V):
        drumCautat[i]=-1;
    print("Drumul are lungimea: ",lungimeDrum) #[0]--sursa   [lungimeDrum]--destinatia
    print("Drum cautat este:",drumCautat)


def constructPath(parent, j):
    global aux
    global lungimeDrum

    if parent[j] == -1:
        drumCautat[lungimeDrum]=j
        lungimeDrum+=1
        return
    constructPath(parent, parent[j])
    drumCautat[lungimeDrum] = j
    lungimeDrum += 1


def printSolution(dist,V,parent,src,dest):
    global aux
    global lungimeDrum

    print("Drumul Cost")

    for i in range(0,V):
        if i == dest:
            print(src, i, "cu costul" ,dist[i])
            constructPath(parent, i)

def minDistance(dist, sptSet):
    global V

    min = 9999999
    min_index = 0

    for v in range(0, V):
        if (sptSet[v] is False) and (dist[v] <= min):
            min = dist[v]
            min_index = v

    return min_index


def dijkstra(graph, src, dest):
    global V
    global lungimeDrum
    dist = [0] * V
    parent = [0] * V
    sptSet = [None] * V

    lungimeDrum = 0
    for i in range(0, V):
        drumCautat[i] = 0

    for i in range(0, V):
        parent[src] = -1
        dist[i] = 9999999
        sptSet[i] = False

    dist[src] = 0

    for count in range(0, V-1):
        u = minDistance(dist, sptSet)
        sptSet[u] = True

        for v in range(0, V):
            if (sptSet[v] is False) and (graph[u][v] != 0) and (dist[u] + graph[u][v] < dist[v]):
                parent[v] = u
                dist[v] = dist[u] + graph[u][v]

    printSolution(dist, V, parent, src, dest)

def determinareServo(dinspre,actual,inspre):
    global noduriParcurse
    print("NODUL ACTUAL",actual)
    if (dinspre == 2 and actual == 1 and inspre == 4 )    
        ser.write(struct.pack('>B',1))
    else if (dinspre == 2 and actual == 1 and inspre == 20 )
        ser.write(struct.pack('>B',0))
    else if (dinspre == 4 and actual == 1 and inspre == 2 )    
        ser.write(struct.pack('>B',2))
    else if (dinspre == 4 and actual == 1 and inspre == 20 )
        ser.write(struct.pack('>B',3))
    else if (dinspre == 20 and actual == 1 and inspre == 2 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 20 and actual == 1 and inspre == 4 )
        ser.write(struct.pack('>B',4))
    else if (dinspre == 3 and actual == 2 and inspre == 1 )    
        ser.write(struct.pack('>B',5))
    else if (dinspre == 1 and actual == 2 and inspre == 3 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 2 and actual == 3 and inspre == 6 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 2 and actual == 3 and inspre == 21 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 6 and actual == 3 and inspre == 2 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 6 and actual == 3 and inspre == 21 )
        ser.write(struct.pack('>B',5))
    else if (dinspre == 21 and actual == 3 and inspre == 6 )    
        ser.write(struct.pack('>B',6))
    else if (dinspre == 21 and actual == 3 and inspre == 2 )
        ser.write(struct.pack('>B',5))
    else if (dinspre == 1 and actual == 4 and inspre == 5 )    
        ser.write(struct.pack('>B',7))
    else if (dinspre == 1 and actual == 4 and inspre == 9 )
        ser.write(struct.pack('>B',8))
    else if (dinspre == 5 and actual == 4 and inspre == 1 )    
        ser.write(struct.pack('>B',8))
    else if (dinspre == 5 and actual == 4 and inspre == 9 )
        ser.write(struct.pack('>B',4))
    else if (dinspre == 9 and actual == 4 and inspre == 1 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 9 and actual == 4 and inspre == 5 )
        ser.write(struct.pack('>B',3))
    else if (dinspre == 4 and actual == 5 and inspre == 23 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 4 and actual == 5 and inspre == 22 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 22 and actual == 5 and inspre == 4 )    
        ser.write(struct.pack('>B',5))
    else if (dinspre == 22 and actual == 5 and inspre == 23 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 23 and actual == 5 and inspre == 4 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 23 and actual == 5 and inspre == 22 )
        ser.write(struct.pack('>B',5))
    else if (dinspre == 3 and actual == 6 and inspre == 10 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 3 and actual == 6 and inspre == 21 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 10 and actual == 6 and inspre == 3 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 10 and actual == 6 and inspre == 21 )
        ser.write(struct.pack('>B',5))
    else if (dinspre == 21 and actual == 6 and inspre == 3 )    
        ser.write(struct.pack('>B',5))
    else if (dinspre == 21 and actual == 6 and inspre == 10 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 8 and actual == 7 and inspre == 22 )    
        ser.write(struct.pack('>B',5))
    else if (dinspre == 8 and actual == 7 and inspre == 23 )
        ser.write(struct.pack('>B',0))
    else if (dinspre == 8 and actual == 7 and inspre == 24 )    
        ser.write(struct.pack('>B',6))
    else if (dinspre == 22 and actual == 7 and inspre == 8 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 22 and actual == 7 and inspre == 23 )    
        ser.write(struct.pack('>B',5))
    else if (dinspre == 22 and actual == 7 and inspre == 24 )
        ser.write(struct.pack('>B',0))
    else if (dinspre == 23 and actual == 7 and inspre == 8 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 23 and actual == 7 and inspre == 22 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 23 and actual == 7 and inspre == 24 )    
        ser.write(struct.pack('>B',5))
    else if (dinspre == 24 and actual == 7 and inspre == 8 )
        ser.write(struct.pack('>B',5))
    else if (dinspre == 24 and actual == 7 and inspre == 22 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 24 and actual == 7 and inspre == 23 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 7 and actual == 8 and inspre == 20 )    
        ser.write(struct.pack('>B',6))
    else if (dinspre == 7 and actual == 8 and inspre == 25 )
        ser.write(struct.pack('>B',5))
    else if (dinspre == 20 and actual == 8 and inspre == 25 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 20 and actual == 8 and inspre == 7 )
        ser.write(struct.pack('>B',5))
    else if (dinspre == 25 and actual == 8 and inspre == 20 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 25 and actual == 8 and inspre == 7 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 4 and actual == 9 and inspre == 12 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 4 and actual == 9 and inspre == 11 )
        ser.write(struct.pack('>B',3))
    else if (dinspre == 11 and actual == 9 and inspre == 4 )    
        ser.write(struct.pack('>B',4))
    else if (dinspre == 11 and actual == 9 and inspre == 12 )
        ser.write(struct.pack('>B',8))
    else if (dinspre == 12 and actual == 9 and inspre == 4 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 12 and actual == 9 and inspre == 11 )
        ser.write(struct.pack('>B',7))
    else if (dinspre == 6 and actual == 10 and inspre == 11 )    
        ser.write(struct.pack('>B',6))
    else if (dinspre == 6 and actual == 10 and inspre == 27 )
        ser.write(struct.pack('>B',0))
    else if (dinspre == 11 and actual == 10 and inspre == 6 )    
        ser.write(struct.pack('>B',5))
    else if (dinspre == 11 and actual == 10 and inspre == 27 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 27 and actual == 10 and inspre == 6 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 27 and actual == 10 and inspre == 11 )
        ser.write(struct.pack('>B',5))
    else if (dinspre == 9 and actual == 11 and inspre == 10 )    
        ser.write(struct.pack('>B',5))
    else if (dinspre == 9 and actual == 11 and inspre == 25 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 10 and actual == 11 and inspre == 25 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 10 and actual == 11 and inspre == 9 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 25 and actual == 11 and inspre == 9 )    
        ser.write(struct.pack('>B',5))
    else if (dinspre == 25 and actual == 11 and inspre == 10 )
        ser.write(struct.pack('>B',0))
    else if (dinspre == 9 and actual == 12 and inspre == 13 )    
        ser.write(struct.pack('>B',6))
    else if (dinspre == 9 and actual == 12 and inspre == 14 )
        ser.write(struct.pack('>B',5))
    else if (dinspre == 13 and actual == 12 and inspre == 9 )    
        ser.write(struct.pack('>B',5))
    else if (dinspre == 13 and actual == 12 and inspre == 14 )
        ser.write(struct.pack('>B',0))
    else if (dinspre == 14 and actual == 12 and inspre == 9 )    
        ser.write(struct.pack('>B',6))
    else if (dinspre == 14 and actual == 12 and inspre == 13 )
        ser.write(struct.pack('>B',0))
    else if (dinspre == 12 and actual == 13 and inspre == 14 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 12 and actual == 13 and inspre == 24 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 14 and actual == 13 and inspre == 24 )    
        ser.write(struct.pack('>B',5))
    else if (dinspre == 14 and actual == 13 and inspre == 12 )
        ser.write(struct.pack('>B',0))
    else if (dinspre == 24 and actual == 13 and inspre == 12 )    
        ser.write(struct.pack('>B',5))
    else if (dinspre == 24 and actual == 13 and inspre == 14 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 16 and actual == 15 and inspre == 27 )    
        ser.write(struct.pack('>B',5))
    else if (dinspre == 16 and actual == 15 and inspre == 30 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 27 and actual == 15 and inspre == 16 )    
        ser.write(struct.pack('>B',6))
    else if (dinspre == 27 and actual == 15 and inspre == 30 )
        ser.write(struct.pack('>B',0))
    else if (dinspre == 30 and actual == 15 and inspre == 16 )    
        ser.write(struct.pack('>B',5))
    else if (dinspre == 30 and actual == 15 and inspre == 27 )
        ser.write(struct.pack('>B',0))
    else if (dinspre == 15 and actual == 16 and inspre == 17 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 15 and actual == 16 and inspre == 26 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 17 and actual == 16 and inspre == 15 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 17 and actual == 16 and inspre == 26 )
        ser.write(struct.pack('>B',5))
    else if (dinspre == 26 and actual == 16 and inspre == 15 )    
        ser.write(struct.pack('>B',5))
    else if (dinspre == 26 and actual == 16 and inspre == 17 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 16 and actual == 17 and inspre == 14 )    
        ser.write(struct.pack('>B',6))
    else if (dinspre == 16 and actual == 17 and inspre == 19 )
        ser.write(struct.pack('>B',5))
    else if (dinspre == 16 and actual == 17 and inspre == 28 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 14 and actual == 17 and inspre == 16 )
        ser.write(struct.pack('>B',5))
    else if (dinspre == 14 and actual == 17 and inspre == 19 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 14 and actual == 17 and inspre == 28 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 19 and actual == 17 and inspre == 14 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 19 and actual == 17 and inspre == 16 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 19 and actual == 17 and inspre == 28 )    
        ser.write(struct.pack('>B',5))
    else if (dinspre == 28 and actual == 17 and inspre == 14 )
        ser.write(struct.pack('>B',5))
    else if (dinspre == 28 and actual == 17 and inspre == 16 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 28 and actual == 17 and inspre == 19 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 28 and actual == 18 and inspre == 25 )    
        ser.write(struct.pack('>B',6))
    else if (dinspre == 25 and actual == 18 and inspre == 28 )
        ser.write(struct.pack('>B',5))
    else if (dinspre == 17 and actual == 19 and inspre == 29 )    
        ser.write(struct.pack('>B',5))
    else if (dinspre == 29 and actual == 19 and inspre == 17 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 1 and actual == 20 and inspre == 8 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 8 and actual == 20 and inspre == 1 )
        ser.write(struct.pack('>B',0))
    else if (dinspre == 3 and actual == 21 and inspre == 6 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 6 and actual == 21 and inspre == 3 )
        ser.write(struct.pack('>B',0))
    else if (dinspre == 5 and actual == 22 and inspre == 7 )    
        ser.write(struct.pack('>B',5))
    else if (dinspre == 7 and actual == 22 and inspre == 5 )
        ser.write(struct.pack('>B',6))
    else if (dinspre == 5 and actual == 23 and inspre == 7 )    
        ser.write(struct.pack('>B',2))
    else if (dinspre == 7 and actual == 23 and inspre == 5 )
        ser.write(struct.pack('>B',1))
    else if (dinspre == 7 and actual == 24 and inspre == 13 )    
        ser.write(struct.pack('>B',1))
    else if (dinspre == 13 and actual == 24 and inspre == 7 )
        ser.write(struct.pack('>B',2))
    else if (dinspre == 18 and actual == 25 and inspre == 8 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 8 and actual == 25 and inspre == 18 )
        ser.write(struct.pack('>B',0))
    else if (dinspre == 11 and actual == 26 and inspre == 16 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 16 and actual == 26 and inspre == 11 )
        ser.write(struct.pack('>B',0))
    else if (dinspre == 10 and actual == 27 and inspre == 15 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 15 and actual == 27 and inspre == 10 )
        ser.write(struct.pack('>B',0))
    else if (dinspre == 17 and actual == 28 and inspre == 18 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 18 and actual == 28 and inspre == 17 )
        ser.write(struct.pack('>B',0))
    else if (dinspre == 30 and actual == 29 and inspre == 19 )    
        ser.write(struct.pack('>B',0))
    else if (dinspre == 19 and actual == 29 and inspre == 30 )
        ser.write(struct.pack('>B',0))
    else if (dinspre == 15 and actual == 30 and inspre == 29 )    
        ser.write(struct.pack('>B',2))
    else if (dinspre == 29 and actual == 30 and inspre == 15 )
        ser.write(struct.pack('>B',1))
    else 
        ser.write(struct.pack('>B',0))


def computareDrum(init):
   global noduriParcurse
   global lungimeDrum
   lungimeDrum-=1
   i=0
   val1 = 60
   val2 = 60
   while(i<=lungimeDrum-1):
       time.sleep(1)
       val1=val2
       val2=reader.get_qr_code()
       print(val1,val2)
       if(val1!=val2 and val2!=60):
            i+=1
            if(i==0): determinareServo(init,drumCautat[i],drumCautat[i+1])
            if(i!=0): determinareServo(drumCautat[i-1],drumCautat[i],drumCautat[i+1])
            if(i==lungimeDrum):
                noduriParcurse[drumCautat[i]]+=1
                if(noduriParcurse[drumCautat[i]]==1): #semnalizare
                    blabla=0
            

def pornire_camera():
    cameraThread=threading.Thread(target=reader.qr_reader)
    cameraThread.start()

def main():
    graph=[[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #0
           [0, 0, 135, 0, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 150, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #1
           [0, 135, 0, 40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #2
           [0, 0, 40, 0, 0, 0, 70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 70, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #3
           [0, 52, 0, 0, 0, 40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #4
           [0, 0, 0, 0, 40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 70, 44, 0, 0, 0, 0, 0, 0, 0],  #5
           [0, 0, 0, 70, 0, 0, 0, 0, 0, 0, 137, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 70, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #6
           [0, 0, 0, 0, 0, 0, 0, 0, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 70, 68, 0, 0, 0, 0, 0, 0],  #7
           [0, 0, 0, 0, 0, 0, 0, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 170, 0, 0, 0, 0, 137, 0, 0, 0, 0, 0],  #8
           [0, 0, 0, 0, 150, 0, 0, 0, 0, 0, 0, 90, 85, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #9
           [0, 0, 0, 0, 0, 0, 137, 0, 0, 0, 0, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 72, 0, 0, 0],  #10
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 90, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 110, 0, 0, 0, 0],  #11
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 85, 0, 0, 0, 60, 140, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #12
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 60, 0, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 40, 0, 0, 0, 0, 0, 0],  #13
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 140, 100, 0, 0, 0, 35, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #14
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 120, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 65, 0, 0, 63],  #15
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 120, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 65, 0, 0, 0, 0],  #16
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 35, 0, 137, 0, 0, 65, 0, 0, 0, 0, 0, 0, 0, 0, 67, 0, 0],  #17
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 137, 0, 0, 70, 0, 0],  #18
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 65, 0, 0, 0, 0, 0, 0, 0, 137, 0, 0, 0, 0, 0],  #19
           [0, 150, 0, 0, 0, 0, 0, 0, 170, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #20
           [0, 0, 0, 70, 0, 0, 70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #21
           [0, 0, 0, 0, 0, 70, 0, 70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #22
           [0, 0, 0, 0, 0, 44, 0, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #23
           [0, 0, 0, 0, 0, 0, 0, 68, 0, 0, 0, 0, 0, 40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #24
           [0, 0, 0, 0, 0, 0, 0, 0, 137, 0, 0, 0, 0, 0, 0, 0, 0, 0, 137, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #25
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 110, 0, 0, 0, 0, 65, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #26
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 72, 0, 0, 0, 0, 65, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #27
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 67, 70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #28
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 137, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 115],  #29
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 63, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 115, 0],  #30



           graph=[[0, 2, 0, 3, 0],#0
           [2, 0, 3, 1, 0],#1
           [0, 3, 0, 4, 0],#2
           [3, 1, 4, 0, 4],#3
           [0, 0, 0, 4, 0]]#4

   # dijkstra(graph, 2, 0)
   # afisareDrum()
    pornire_camera()
    noduriCerute = [0] * 4
    citiri=0
    primulAjuns=-1
    val1=60 #nu citim QR Code de pe camera
    val2=60
    trimis=0
    # 1) noduriCerute=citesc cele 4 noduri (citiri=1)
    # 2)init=citesc primul qrcode (citiri=2)
    # 3)primulAjuns=citesc al doilea qrcode (citiri=3)
    while(True):
        time.sleep(2)
        val1=val2
        val2=reader.get_qr_code()
        print(val1, val2)
        if (val2 == 60 and val1 == 60 and trimis == 0):
            comanda=1
            trimis+=1
            ser.write(struct.pack('>B',comanda))
        if (val2 == 60 and val1 == 60 and trimis > 0):
            comanda=0
            ser.write(struct.pack('>B',comanda))
        if(val2!=60 and val1!=val2):
            citiri+=1
        if(citiri==1 and val2!=60):
            noduriCerute=val2.split("_")  #cazul in care avem QR Code-ul cu nodurile EX:43_56_20_30
        if(citiri==2 and val2!=60):
            init=int(val2)
        if(citiri==3):
            primulAjuns=int(val2)
            break

    print(noduriCerute)
    print(init)
    print(primulAjuns)

    for i in range(-1, 3):
        print("ITERATIE ",i)
        if(i==-1):
            src = primulAjuns
            dest = int(noduriCerute[0])
            dijkstra(graph, src, dest)
        if(i!=-1):
            init=drumCautat[lungimeDrum-1]
            src = int(noduriCerute[i])
            dest = int(noduriCerute[i+1])
            dijkstra(graph, src, dest)
        computareDrum(init)

if __name__=="__main__":
    main()
