#include <Servo.h>
#include <QTRSensors.h>

#define Kp 0
#define Kd 0
#define Ki 0
#define NUM_SENSORS             6  // number of sensors used
#define NUM_SAMPLES_PER_SENSOR  4  // average 4 analog samples per sensor reading
#define EMITTER_PIN             2  // emitter is controlled by digital pin 2
#define rightMotor1 6
#define rightMotor2 4
#define MotorPWM 11
#define leftMotor1 3
#define leftMotor2 5
#define speedul 20
#define speedspate 20

Servo myservo;

QTRSensorsAnalog qtra((unsigned char[]) {
  0, 1, 2, 3, 4, 5
},
NUM_SENSORS, NUM_SAMPLES_PER_SENSOR, EMITTER_PIN);
unsigned int sensorValues[NUM_SENSORS];

void setup() {
  Serial.begin(9600);
  pinMode(rightMotor1, OUTPUT); //A
  pinMode(rightMotor2, OUTPUT); //B
  pinMode(MotorPWM, OUTPUT);
  pinMode(leftMotor1, OUTPUT);
  pinMode(leftMotor2, OUTPUT);
  Serial.println("pornit serial");
  myservo.attach(9);
  delay(20);
  
  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);    // turn on Ar1duino's LED to indicate we are in calibration mode
  for (int i = 0; i < 400; i++)  // make the calibration take about 10 seconds
  {
    qtra.calibrate();       // reads all sensors 10 times at 2.5 ms per six sensors (i.e. ~25 ms per call)
  }
  digitalWrite(13, LOW);     // turn off Arduino's LED to indicate we are through with calibration

  

  // print the calibration minimum values measured when emitters were on
 
  Serial.begin(9600);
  for (int i = 0; i < NUM_SENSORS; i++)
  {
    Serial.print(qtra.calibratedMinimumOn[i]);
    Serial.print(' ');
  }
  Serial.println();

  // print the calibration maximum values measured when emitters were on
  for (int i = 0; i < NUM_SENSORS; i++)
  {
    Serial.print(qtra.calibratedMaximumOn[i]);
    Serial.print(' ');
  }
  Serial.println();
  Serial.println();
  delay(1500);

}
void motoare()
{
  digitalWrite(rightMotor1, HIGH);
  digitalWrite(rightMotor2, LOW);
  analogWrite(MotorPWM, speedul);
  digitalWrite(leftMotor1, LOW);
  digitalWrite(leftMotor2, HIGH);
}
void dam_cu_spatele()
{
 //dam cu spatele
 //digitalWrite(rightMotor1, LOW);
  //digitalWrite(rightMotor2, HIGH);
  //analogWrite(MotorPWM, speedspate);
  //digitalWrite(leftMotor1, HIGH);
  //digitalWrite(leftMotor2, LOW);
  }
void urmareste_linia()
{
  
   unsigned int sensors[6];
  // read calibrated sensor values and obtain a measure of the line position from 0 to 5000  To get raw sensor values, call:
  //  qtra.read(sensorValues); instead of unsigned int position = qtra.readLine(sensorValues);
  qtra.read(sensorValues);
  //5-cel mai stanga 4-3-2 1-cel mai din dreapta

  for (unsigned char i = 0; i < NUM_SENSORS; i++)
  {
    Serial.print(sensorValues[i]);
    Serial.print('\t');
  }
  Serial.println(); // uncomment this line if you are using raw values
  //Serial.println(position); // comment this line out if you are using raw values
  delay(200);
  //Serial.print("ok1");
  
  if ((sensorValues[5] > 600) && (sensorValues[4] < 150) && (sensorValues[3] < 150) && (sensorValues[2] < 150) && (sensorValues[1] < 150) && (sensorValues[0] < 150))
  {
   // for (int pos=70;pos<=85;pos++)
   // {
    //  myservo.write(85);
    //  delay(2);
   // }
    myservo.write(85);
    delay(15);
  }
  else if ((sensorValues[5] > 600) && (sensorValues[4] > 600) && (sensorValues[3] < 150) && (sensorValues[2] < 150) && (sensorValues[1] < 150) && (sensorValues[0] < 150))
  {
    myservo.write(80);
    delay(15);
  }
  else if ((sensorValues[5] < 150) && (sensorValues[4] > 600) && (sensorValues[3] < 150) && (sensorValues[2] < 150) && (sensorValues[1] < 150) && (sensorValues[0] < 150))
  {
    myservo.write(80);
    delay(15);
  }
  else if ((sensorValues[5] < 150) && (sensorValues[4] > 600) && (sensorValues[3] > 600) && (sensorValues[2] < 150) && (sensorValues[1] < 150) && (sensorValues[0] < 150))
  {
    myservo.write(75);
    delay(15);
  }
  else if ((sensorValues[5] < 150) && (sensorValues[4] < 150) && (sensorValues[3] > 600) && (sensorValues[2] < 150) && (sensorValues[1] < 150) && (sensorValues[0] < 150))
  {
    myservo.write(75);
    delay(15);
  }
  else if ((sensorValues[5] < 150) && (sensorValues[4] < 150) && (sensorValues[3] > 600) && (sensorValues[2] > 600) && (sensorValues[1] < 120) && (sensorValues[0] < 150))
  {
    myservo.write(70);
   delay(15);
  }
  else if ((sensorValues[5] < 150) && (sensorValues[4] < 150) && (sensorValues[3] < 150) && (sensorValues[2] > 600) && (sensorValues[1] > 600) && (sensorValues[0] < 150))
  {
    myservo.write(65);
   delay(15);
  }
   else if ((sensorValues[5] < 150) && (sensorValues[4] < 150) && (sensorValues[3] < 150) && (sensorValues[2] > 600) && (sensorValues[1] < 150) && (sensorValues[0] < 150))
  {
    myservo.write(65);
   delay(15);
  }
  else if ((sensorValues[5] < 150) && (sensorValues[4] < 150) && (sensorValues[3] < 150) && (sensorValues[2] < 150) && (sensorValues[1] > 600) && (sensorValues[0] >600))
  {
    myservo.write(60);
   delay(15);
  }
   else if ((sensorValues[5] < 150) && (sensorValues[4] < 150) && (sensorValues[3] < 150) && (sensorValues[2] < 150) && (sensorValues[1] > 600) && (sensorValues[0] < 150))
  {
    myservo.write(60);
   delay(15);
  }
  else if ((sensorValues[5] < 150) && (sensorValues[4] < 150) && (sensorValues[3] < 150) && (sensorValues[2] < 150) && (sensorValues[1] < 150) && (sensorValues[0] > 600))
  {
    myservo.write(55);
   delay(15);
  }
  else if ((sensorValues[5] < 150) && (sensorValues[4] < 150) && (sensorValues[3] < 150) && (sensorValues[2] < 150) && (sensorValues[1] < 150) && (sensorValues[0] < 150))
  {
   myservo.write(70);
   delay(15);
   //dam_cu_spatele();
   delay(700);
  } 
  else if ((sensorValues[5] > 600) && (sensorValues[4] > 600) && (sensorValues[3] > 600) && (sensorValues[2] > 600) && (sensorValues[1] > 600) && (sensorValues[0] > 600))
  {
   myservo.write(70);
   //Serial.print("ok2");
   delay(15);
   //stopem();
   delay(500);
  } 
}

/*void decizie()
{
  while (Serial.available() > 0)
     {
     int comanda=Serial.parseInt();
          if (comanda == 0)
          {
              myservo.write(70);
              delay(500);
          }
          else  if (comanda == 1)
          {
              myservo.write(45);
              delay(500);//dreapta
          }
           else  if (comanda == 2)
          {
              myservo.write(95); //stanga
              delay(500);
          }
           else  if (comanda == 3)
          {
            myservo.write(60); //dreapta usor
            delay(300);
          }
           else  if (comanda == 4)
          {
           myservo.write(80); //stanga usor
              delay(300);
          }
           else  if (comanda == 5)
          {
             myservo.write(110); //stanga tare
             delay(1000);
          }
           else  if (comanda == 6)
          {
            myservo.write(30); //dreapta tare
            delay(1000);
           //dreapta cerc
          }
           else  if (comanda == 7)
          {
            dam_cu_spatele();
            // spate
          }    
       }
}
*/

void loop()
{ 
 // motoare();
  urmareste_linia();
 // for (int pos = 70; pos <= 90; pos += 1) { // goes 70 e drept
    // in steps of 1 degree
    //myservo.write(pos);              // tell servo to go to position in variable 'pos'
   // delay(15);                       // waits 15ms for the servo to reach the position
 // }
  //myservo.write(70);
  //delay(15);
  //decizie();
  delay(10);
}
